﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneController : MonoBehaviour
{
    public struct Axis
    {
        public Axis(bool set)
        {
            up = set;
            down = set;
            fwd = set;
            bwd = set;
            left = set;
            right = set;
            rrot = set;
            lrot = set;
        }
        public bool up;
        public bool down;
        public bool fwd;
        public bool bwd;
        public bool left;
        public bool right;
        public bool rrot;
        public bool lrot;
    }
    public float thr = 98.1f;
    public float MaxThrust = 1500.0f;
    public float yawSpeed = 150.0f;
    public float maxAngle= 30.0f;
    public AltHolder altHold;

    private Axis axis = new Axis(false);
    private Rigidbody rb;
    private float xdiff;
    private float ydiff;
    private float zdiff;
    private Vector3 locVel;
    
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        altHold = GetComponent<AltHolder>();

    }

    // Update is called once per frame
    void Update()
    {
        InputKeys();

    }
    private void FixedUpdate()
    {
        DroneComputer();
    }

    private void InputKeys()
    {
        axis.up = Input.GetKey(KeyCode.Space);
        axis.down = Input.GetKey(KeyCode.Z);
        axis.fwd = Input.GetKey(KeyCode.W);
        axis.bwd = Input.GetKey(KeyCode.S);
        axis.left = Input.GetKey(KeyCode.A);
        axis.right = Input.GetKey(KeyCode.D);
        axis.lrot = Input.GetKey(KeyCode.Q);
        axis.rrot = Input.GetKey(KeyCode.E);
    }
    private void DroneComputer()
    {
        locVel = transform.InverseTransformDirection(rb.velocity);

        //left/right rotation (yaw)
        if ((!axis.rrot && !axis.lrot) || (axis.rrot && axis.lrot))
            ydiff = -rb.angularVelocity.y*100;
        else
            ydiff = (axis.rrot ? 1 : -1) * yawSpeed;
        rb.AddRelativeTorque(Vector3.up * ydiff);

        //nose up/down rotation (pitch)
        if ((!axis.fwd && !axis.bwd) || (axis.fwd && axis.bwd))
        {
            if (Mathf.Abs(locVel.z) > 0.1)
                xdiff = (Mathf.Abs(locVel.z) > maxAngle ? maxAngle * Mathf.Sign(-locVel.z) : -locVel.z);
            else
                xdiff = 0f;

        }
        else
            xdiff = (axis.fwd ? maxAngle : -maxAngle);
        if ((Mathf.Abs((transform.eulerAngles.x > 180 ? transform.eulerAngles.x - 360 : transform.eulerAngles.x)) < Mathf.Abs(xdiff)) || Mathf.Sign((transform.eulerAngles.x > 180 ? transform.eulerAngles.x - 360 : transform.eulerAngles.x)) != Mathf.Sign(xdiff))
            transform.Rotate(Vector3.right, (xdiff-(transform.eulerAngles.x>180 ? transform.eulerAngles.x-360: transform.eulerAngles.x))/10, Space.Self);

        //left/right tilt (roll)
        if ((!axis.right && !axis.left) || (axis.right && axis.left))
        {
            if (Mathf.Abs(locVel.x) > 0.1)
                zdiff = (Mathf.Abs(locVel.x) > maxAngle ? maxAngle * Mathf.Sign(locVel.x) : locVel.x);
            else
                zdiff = 0f;
        }
        else
            zdiff = (axis.right ? -maxAngle : maxAngle);
        if ((Mathf.Abs(transform.rotation.eulerAngles.x) < Mathf.Abs(zdiff))|| Mathf.Sign(transform.rotation.eulerAngles.x) != Mathf.Sign(zdiff))
            transform.Rotate(Vector3.forward, (zdiff - (transform.eulerAngles.z > 180 ? transform.eulerAngles.z - 360 : transform.eulerAngles.z)) / 10, Space.Self);

        //thrust
        if (!((!axis.up && !axis.down) || (axis.up && axis.down)))
            altHold.set_height += (axis.up ? 0.1f : -0.1f);
        rb.AddRelativeForce(Vector3.up*thr);
    }
}
