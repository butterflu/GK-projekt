﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollower : MonoBehaviour
{
    public GameObject target;
    public Vector3 offset = new Vector3(0,5,-18);
    void LateUpdate()
    {
        transform.position = target.transform.position + offset;
        transform.LookAt(target.transform);
    }
}
