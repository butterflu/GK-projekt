﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltHolder : MonoBehaviour
{
    public bool is_on = true;
    public DroneController controler;
    public float set_height;
    public Rigidbody rb;

    private float vel;
    private float h;
    private float hdiff;
    // Start is called before the first frame update
    void Start()
    {
        controler = GetComponent<DroneController>();
        rb = GetComponent<Rigidbody>();
        if (!is_on){
            gameObject.active = false;
        }
        set_height = controler.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        h = gameObject.transform.position.y;
        vel = rb.velocity.y;
        hdiff = h - set_height;
        controler.thr = 98.1f+(-hdiff-vel)*10f;
        
    }
}
